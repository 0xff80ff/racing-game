﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIVehiclesStats : MonoBehaviour
{
	public UIVehicleInfo infoPrefab;

	readonly Dictionary<Transform, UIVehicleInfo> infos = new Dictionary<Transform, UIVehicleInfo>();

	public void OnTrackProgress(Transform vehicle, Track.ProgressInfo info)
	{
		UIVehicleInfo uiInfo;
		if (!infos.TryGetValue(vehicle, out uiInfo))
		{
			infos.Add(vehicle, uiInfo = Instantiate(infoPrefab, transform));

			uiInfo.name.text = vehicle.name;
		}
		
		var gameplay = Gameplay.instance;
		if (gameplay == null)
			return;

		if (0 < info.loopsCount && info.loopsCount <= gameplay.loopsCount && info.checkPointIndex == 0)
			uiInfo.time.text = (info.lastCheckPointTime - gameplay.startTime).ToString("N1");

		if (info.loopsCount < gameplay.loopsCount)
			uiInfo.loop.text = (info.loopsCount + 1).ToString();
	}
}
