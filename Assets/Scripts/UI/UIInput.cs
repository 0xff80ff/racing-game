﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInput : MonoBehaviour
{
	static Vector2 _direction;

	public static Vector2 direction {
		get
		{
			var direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) + _direction;
			direction.x = Mathf.Clamp(direction.x, -1, 1);
			direction.y = Mathf.Clamp(direction.y, -1, 1);
			return direction;
		}
	}

	public float horizontal { set { _direction.x = value; } }
	public float vertical { set { _direction.y = value; } }

	private void OnDestroy()
	{
		horizontal = vertical = 0;
	}
}
