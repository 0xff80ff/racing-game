﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour
{
	public void GameplayStart()
	{
		Gameplay.instance.DoStart();
	}

	public void GameplayRestart()
	{
		Gameplay.instance.DoRestart();
	}
}
