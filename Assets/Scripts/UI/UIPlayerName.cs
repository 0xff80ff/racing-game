﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: make it as element of canvas (with position as a projection of 3D to canvas)
[RequireComponent(typeof(TextMesh))]
public class UIPlayerName : MonoBehaviour
{
	Transform mainCameraTransform;

	TextMesh text;

	void Awake ()
	{
		text = GetComponent<TextMesh>();

		mainCameraTransform = Camera.main.transform;
	}

	void Start()
	{
		text.text = transform.parent.name;
	}

	void Update ()
	{
		transform.rotation = mainCameraTransform.rotation;
	}
}
