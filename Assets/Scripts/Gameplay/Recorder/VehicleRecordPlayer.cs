﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Vehicle))]
public class VehicleRecordPlayer : MonoBehaviour
{
	[Range(0, 5)]
	public float fadeInTime = 2;

	Vehicle vehicle;

	private void Awake()
	{
		vehicle = GetComponent<Vehicle>();
	}

	private void Start()
	{
		Gameplay.instance.onStart.AddListener(StartPlaying);
	}

	private void StartPlaying()
	{
		if (playingRoutine != null)
			return;

		playingRoutine = StartCoroutine(Playing());
	}

	Coroutine playingRoutine = null;
	IEnumerator Playing()
	{
		var records = VehicleRecords.instance;
		if (records == null)
		{
			Destroy(gameObject);
			yield break;
		}

		VehicleRecords.Record record = records.randomRecord;
		if (record == null)
		{
			Destroy(gameObject);
			yield break;
		}

		var rb = vehicle.GetComponent<Rigidbody>();

		float playingTime = 0;

		var startPosition = rb.position;
		var startRotation = rb.rotation;

		foreach (var snapshot in record.snapshots)
		{
			if (playingTime < fadeInTime)
			{
				float mixRatio = Mathf.Pow(playingTime / fadeInTime, 3);

				rb.position = Vector3.Lerp(startPosition, snapshot.position, mixRatio);
				rb.rotation = Quaternion.Lerp(startRotation, snapshot.rotation, mixRatio);

				if (mixRatio > 0.9f)
					Debug.Log(name);
			}
			else
			{
				rb.position = snapshot.position;
				rb.rotation = snapshot.rotation;
			}

			playingTime += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}

		var lastPosition = record.snapshots[record.snapshots.Count - 1].position;
		var prelastPosition = record.snapshots[record.snapshots.Count - 2].position;

		rb.velocity = (lastPosition - prelastPosition) / Time.fixedDeltaTime;

		playingRoutine = null;
	}
}
