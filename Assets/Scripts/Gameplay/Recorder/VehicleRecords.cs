﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class VehicleRecords : MonoBehaviour
{
	public static VehicleRecords instance { get; private set; }

	[System.Serializable]
	public class Record
	{
		// TODO: record deltas
		// TODO: record delta time
		[System.Serializable]
		public class Snapshot
		{
			const float axisMax = 128;
			const float axisToShort = short.MaxValue / axisMax;
			const float shortToAxis = 1 / axisToShort;
			const float angleToByte = byte.MaxValue / 360.0f;
			const float byteToAngle = 1 / angleToByte;

			short posX, posY, posZ;
			byte rotX, rotY, rotZ;

			public Snapshot(Transform t)
			{
				posX = (short)Mathf.RoundToInt(Mathf.Clamp(t.position.x, -axisMax, axisMax) * axisToShort);
				posY = (short)Mathf.RoundToInt(Mathf.Clamp(t.position.y, -axisMax, axisMax) * axisToShort);
				posZ = (short)Mathf.RoundToInt(Mathf.Clamp(t.position.z, -axisMax, axisMax) * axisToShort);

				var eulerAngles = t.rotation.eulerAngles;
				rotX = (byte)Mathf.RoundToInt(Mathf.Repeat(eulerAngles.x, 360) * angleToByte);
				rotY = (byte)Mathf.RoundToInt(Mathf.Repeat(eulerAngles.y, 360) * angleToByte);
				rotZ = (byte)Mathf.RoundToInt(Mathf.Repeat(eulerAngles.z, 360) * angleToByte);
			}

			public Vector3 position { get { return new Vector3(posX, posY, posZ) * shortToAxis; } }

			public Quaternion rotation { get { return Quaternion.Euler(rotX * byteToAngle, rotY * byteToAngle, rotZ * byteToAngle); } }
		}

		public List<Snapshot> snapshots = new List<Snapshot>();
	}

	public readonly List<Record> records = new List<Record>();
	
	public Record randomRecord
	{
		get
		{
			if (records.Count == 0)
				return null;

			int recordIndex = Random.Range(0, records.Count);
			var result = records[recordIndex];
			records.RemoveAt(recordIndex);
			return result;
		}
	}

	string GetRecordPath(int i)
	{
		return string.Format("{0}Record{1}.rec", Application.persistentDataPath, i);
	}

	void Load()
	{
		string recordPath;
		for (int i = 0; File.Exists(recordPath = GetRecordPath(i)); ++i)
			try
			{
				using (var file = File.OpenRead(recordPath))
				{
					BinaryFormatter formatter = new BinaryFormatter();
					var record = (Record)formatter.Deserialize(file);
					records.Add(record);

					Debug.Log("Loaded " + recordPath);
				}
			}
			catch (System.Exception e)
			{
				Debug.LogErrorFormat("Cannot write to {0}: {1}", recordPath, e.Message);
			}
	}

	public void Save(Record record)
	{
		// TODO: use indices file
		string recordPath;
		for (int i = 0; File.Exists(recordPath = GetRecordPath(i)); ++i)
			/* NOP */;

		try
		{
			using (var file = File.OpenWrite(recordPath))
			{
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(file, record);

				Debug.Log("Saved " + recordPath);
			}
		}
		catch (System.Exception e)
		{
			Debug.LogErrorFormat("Cannot write to {0}: {1}", recordPath, e.Message);
		}
	}

	void Awake()
	{
		instance = this;

		Load();
	}
}
