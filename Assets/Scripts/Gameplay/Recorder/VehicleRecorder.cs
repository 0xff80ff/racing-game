﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Vehicle))]
public class VehicleRecorder : MonoBehaviour
{
	Vehicle vehicle;

	private void Awake()
	{
		vehicle = GetComponent<Vehicle>();
	}

	private void Start()
	{
		Gameplay.instance.onStart.AddListener(StartRecord);
		Gameplay.instance.onFinish.AddListener(StopRecord);
	}

	private void StartRecord()
	{
		if (recordingRoutine != null)
			return;

		recordingRoutine = StartCoroutine(Recording());
	}

	Coroutine recordingRoutine = null;
	IEnumerator Recording()
	{
		VehicleRecords.Record record = new VehicleRecords.Record();

		var vehicleTransform = vehicle.transform;

		do
		{
			record.snapshots.Add(new VehicleRecords.Record.Snapshot(vehicleTransform));

			yield return new WaitForFixedUpdate();
		} while (recordingRoutine != null);

		var records = VehicleRecords.instance;

		if (records)
			records.Save(record);
	}

	private void StopRecord()
	{
		recordingRoutine = null;
	}
}
