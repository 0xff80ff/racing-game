﻿using UnityEngine;

public class CheckPoint : MonoBehaviour
{
	public Track container { get; private set; }

	public int index { get { return transform.GetSiblingIndex(); } }

	private void Awake()
	{
		container = GetComponentInParent<Track>();
	}

	private void OnTriggerEnter(Collider other)
	{
		container.Reach(this, other.transform);
	}
}
