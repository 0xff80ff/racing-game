﻿using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Track : MonoBehaviour
{
	public bool debug = false;

	[System.Serializable] public class UnityEventTransformProgressInfo : UnityEvent<Transform, ProgressInfo> {}
	public UnityEventTransformProgressInfo onProgress = new UnityEventTransformProgressInfo();

	public int checkPointsCount { get { return transform.childCount; } }

	[System.Serializable]
	public class ProgressInfo
	{
		public ProgressInfo(Track t)
		{
			track = t;
			checkPointIndex = 0;
			loopsCount = 0;
		}

		public readonly Track track;

		public int checkPointIndex { get; private set; }
		public int loopsCount { get; private set; }
		public float lastCheckPointTime { get; private set; }

		public bool TrySet(int newCheckPointIndex)
		{
			int nextCheckPoint = (checkPointIndex + 1) % track.checkPointsCount;
			if (nextCheckPoint != newCheckPointIndex)
				return false;

			checkPointIndex = nextCheckPoint;
			if (checkPointIndex == 0)
				++loopsCount;

			lastCheckPointTime = Time.timeSinceLevelLoad;

			return true;
		}

		public override string ToString()
		{
			return string.Format("Loops={0}, CheckPoint={1}", loopsCount, checkPointIndex);
		}
	}
	readonly Dictionary<Transform, ProgressInfo> progressInfos = new Dictionary<Transform, ProgressInfo>();

	public void Reach(CheckPoint point, Transform who)
	{
		ProgressInfo info;
		if (progressInfos.TryGetValue(who, out info))
		{
			if (!info.TrySet(point.index))
				return;
		}
		else
			progressInfos.Add(who, info = new ProgressInfo(this));
		
		onProgress.Invoke(who, info);
	}

	private void Awake()
	{
		if (debug)
			onProgress.AddListener((who, info) => Debug.LogFormat("Object '{0}' progress {1}", who, info));
	}
}
