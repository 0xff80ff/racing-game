﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Vehicle))]
public class VehicleControllerInput : MonoBehaviour
{
	public Vehicle vehicle { get; private set; }

	Vector2 controlDirection;

	private void Awake()
	{
		vehicle = GetComponent<Vehicle>();
	}

	private void Start()
	{
		Gameplay.instance.onStart.AddListener(() => enabled = true);
		Gameplay.instance.onFinish.AddListener(() => enabled = false);
		enabled = false;
	}
	
	private void Update()
	{
		controlDirection = UIInput.direction;
	}

	private void FixedUpdate()
	{
		vehicle.Accelerate(controlDirection.y);
		vehicle.Steer(controlDirection.x);
	}

	private void OnDisable()
	{
		vehicle.Accelerate(0);
		vehicle.Steer(0);
	}
}
