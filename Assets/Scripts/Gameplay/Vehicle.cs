﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Vehicle : MonoBehaviour
{
	[Range(0.1f, 1000)]
	public float enginePower = 100;

	[Range(15, 90)]
	public float stearAngle = 60;

	[System.Serializable]
	public class Wheel
	{
		public bool acceleratable;
		public bool steerable;
		public WheelCollider collider;
		public Transform view;
	}
	public Wheel[] wheels;

	new public Rigidbody rigidbody { get; private set; }
	new public Collider collider { get; private set; }

	private void Awake()
	{
		Debug.Assert(wheels.Length > 0, "No wheels assigned for " + name, this);

		rigidbody = GetComponent<Rigidbody>();
		collider = GetComponent<Collider>();
		
		rigidbody.centerOfMass = new Vector3(0, -0.5f * collider.bounds.size.y, 0);
	}

	public void Accelerate(float powerForward)
	{
		powerForward = Mathf.Clamp(powerForward, -1, 1);

		foreach (var wheel in wheels)
			if (wheel.acceleratable)
				wheel.collider.motorTorque = enginePower * powerForward;
	}

	public void Steer(float powerRight)
	{
		powerRight = Mathf.Clamp(powerRight, -1, 1);

		foreach (var wheel in wheels)
			if (wheel.steerable)
				wheel.collider.steerAngle = powerRight * stearAngle;
	}

	private void FixedUpdate()
	{
		foreach (var wheel in wheels)
		{
			Vector3 viewPosition;
			Quaternion viewRotation;
			wheel.collider.GetWorldPose(out viewPosition, out viewRotation);
			wheel.view.SetPositionAndRotation(viewPosition, viewRotation);
		}
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(transform.TransformPoint(GetComponent<Rigidbody>().centerOfMass), 0.1f);
	}
}
