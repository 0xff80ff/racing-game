﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleCamera : CameraWithTarget
{
	[Range(1, 25)]
	public float distance = 10;

	public float height = 8;

	protected override void LateUpdate()
	{
		if (target)
		{
			var direction = target.position - transform.position;
			var goalPosition = target.position - direction.normalized * distance;
			goalPosition.y = height;

			float dumpRatio = Time.unscaledDeltaTime * speed;
			if (dumpRatio < 1)
				transform.position += (goalPosition - transform.position) * dumpRatio;
			else
				transform.position = goalPosition;
		}

		base.LateUpdate();
	}

	public void OnSpawned(Vehicle vehicle)
	{
		if (vehicle.CompareTag("Player"))
			target = vehicle.transform;
	}
}
