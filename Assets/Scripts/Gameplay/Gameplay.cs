﻿using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine;

public class Gameplay : MonoBehaviour
{
	[Range(1, 10)]
	public int loopsCount = 3;

	public VehiclesController vehicles;
	public Track track;

	public static Gameplay instance { get; private set; }
	
	public UnityEvent onStart = new UnityEvent();
	public UnityEvent onFinish = new UnityEvent();
	public UnityEvent onRestart = new UnityEvent();

	public float startTime { get; private set; }
	public bool isPlaying { get; private set; }

	public void DoStart()
	{
		if (isPlaying)
			return;

		startTime = Time.timeSinceLevelLoad;
		isPlaying = true;

		onStart.Invoke();
	}

	public void DoFinish()
	{
		if (!isPlaying)
			return;

		isPlaying = false;

		onFinish.Invoke();
	}

	public void DoRestart()
	{
		isPlaying = false;

		onRestart.Invoke();

		// TODO: use own scene controller to handle async loading, etc.
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void OnTrackProgress(Transform who, Track.ProgressInfo info)
	{
		if (who == vehicles.player.transform && info.loopsCount >= loopsCount)
			DoFinish();
	}

	private void Awake()
	{
		instance = this;
	}

	private void Start()
	{
		vehicles.Spawn(4);
	}
}
