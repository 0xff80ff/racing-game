﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class VehiclesController : MonoBehaviour
{
	// TODO: use player description class
	public string[] nicknames;
	public string randomNickname { get { return nicknames[Random.Range(0, nicknames.Length)]; } }

	public Vehicle vehiclePlayerPrefab;
	public Vehicle vehicleEnemyPrefab;

	public readonly List<Vehicle> vehicles = new List<Vehicle>();
	
	public Vehicle player { get; private set; }

	[System.Serializable] public class UnityEventVehicle : UnityEvent<Vehicle> {}
	public UnityEventVehicle onSpawned = new UnityEventVehicle();

	public void Awake()
	{
		Debug.Assert(transform.childCount > 0);
	}

	public void Spawn(int count)
	{
		int playerIndex = Random.Range(0, count);

		for (int i = 0; i < count; ++i)
			Spawn(i == playerIndex);
	}

	public Vehicle Spawn(bool isPlayer)
	{
		if (vehicles.Count >= transform.childCount)
			return null;

		var spawnPoint = transform.GetChild(vehicles.Count);

		var newVehicle = Instantiate(
			isPlayer ? vehiclePlayerPrefab : vehicleEnemyPrefab,
			spawnPoint.position,
			spawnPoint.rotation
		);

		newVehicle.name = isPlayer ? "Player" : randomNickname;

		vehicles.Add(newVehicle);
		if (isPlayer)
		{
			Debug.Assert(player == null);
			player = newVehicle;
		}

		onSpawned.Invoke(newVehicle);

		return newVehicle;
	}
}
