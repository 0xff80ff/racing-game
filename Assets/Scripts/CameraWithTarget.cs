﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraWithTarget : MonoBehaviour
{
	/// <summary> FPS caused </summary>
	public const float speedMax = 30;

	[Range(0.01f, speedMax)]
	public float speed = 1;

	new public Camera camera { get; private set; }
	
	[SerializeField]
	Transform _target;
	public Transform target
	{
		get { return _target; }
		set { enabled = (_target = value) != null; }
	}

	public LayerMask obstaclesMask;

	float colliderRadius { get { return camera.nearClipPlane * Mathf.Tan(camera.fieldOfView * Mathf.Deg2Rad); } }

	Transform camTransform;

	private void OnValidate()
	{
		target = target;
	}

	private void OnEnable()
	{
		camera = GetComponent<Camera>();

		camTransform = camera.transform;
	}

	protected virtual void LateUpdate()
	{
		if (target == null)
			return;

		Vector3 camToTargetDirection = target.position - camera.transform.position;

		if (camToTargetDirection.sqrMagnitude < 1e-6f)
			return;

		float dumpRatio = Mathf.Min(Time.unscaledDeltaTime, Time.maximumDeltaTime) * speed;

		if (dumpRatio < 1)
			camTransform.rotation = Quaternion.LerpUnclamped(
				camTransform.rotation,
				Quaternion.LookRotation(camToTargetDirection, target.up),
				dumpRatio
			);
		else
			camTransform.rotation = Quaternion.LookRotation(camToTargetDirection, target.up);

		float distance = camToTargetDirection.magnitude;

		RaycastHit hit;
		if (Physics.SphereCast(target.position, colliderRadius, -transform.forward, out hit, distance, obstaclesMask))
			camTransform.position = target.position - camTransform.forward * hit.distance;
	}
}